# Miscellanées

[**chrome.org**](https://gitlab.com/jbbourgoin/jeux-de-role/-/blob/main/divers/chrome.org) : Un jeu de cyberfantasy basé sur QuestWorlds

[**fabuliste.org**](https://gitlab.com/jbbourgoin/jeux-de-role/-/blob/main/divers/fabuliste.org) : Un jeu de rôle léger utilisant les dés FATE.

[**spores.md**](https://gitlab.com/jbbourgoin/jeux-de-role/-/blob/main/divers/spores.md) : Une ébauche d'univers post-apocalyptique se déroulant dans le Finistère.
