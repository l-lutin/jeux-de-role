#+TITLE:Discussions autour du système de résolution

https://basicroleplaying.org/topic/14619-an-hybrid-system-for-extended-contests/

* Un système hybride pour les conflits étendus

** Corvantir

Avertissement : ce billet a été écrit avec HeroQuest : Glorantha
(HQG) à l'esprit mais peut être utilisé presque tel quel avec
QuestWorlds.

J'ai dépassé la quarantaine de sessions avec HQG et il y a un problème
avec le système de concours étendu. Ce n'est pas la faute du système
bien sûr, mais plutôt de mon incapacité à utiliser correctement les
règles des concours notés et/ou à les gérer tout en jouant.

Premier problème : le but/prix et la tactique contraignent l'action,
le cadre ainsi défini devient une sorte de carcan lorsque les concours
sont résolus. J'ai vu beaucoup de concours prolongés devenir un
exercice fade de lancer de dés, un échange après l'autre, jusqu'à ce
que 5RP soient marqués.

Deuxième problème : le système des Points de Résolution (plus
précisément le fait qu'un adversaire n'est éjecté du concours que
lorsqu'un seul compétiteur marque 5 PR contre lui quel que soit le
nombre de PR marqués par les autres compétiteurs) rend les concours
plus longs. Repartir à zéro lorsqu'une nouvelle paire est créée au
cours d'un concours rallonge la durée de la scène sans ajouter quelque
chose de divertissant, à mon avis bien sûr. De plus, beaucoup de
joueurs sont très perturbés par le fait qu'ils doivent marquer tout un
tas de 5RP alors que plusieurs RP ont déjà été marqués par d'autres
concurrents contre le même adversaire.

Désireux de trouver une solution, j'ai testé le système de Points
d'Avantage de HeroQuest 1. Le mécanisme de " transfert " a prouvé
qu'il pouvait être abusé avec les Points de Héros (dans mes jeux, les
Points de Héros sont déconnectés de l'expérience). Si le PA donnait
aux joueurs une meilleure idée de leur statut et de celui de leurs
adversaires lors des combats, les joueurs n'ont pas apprécié le "
métagame " introduit par le système d'enchères.

Après une nouvelle session au cours de laquelle un concours marqué
s'est avéré une fois de plus frustrant, j'ai imaginé une solution
combinant ce que je pense être le meilleur des deux mondes : des
points d'avantage avec des montants fixes de pertes de PA en fonction
des niveaux de victoire.

Les Points Avantage sont renommés Points de Stress (PS)

La réserve de départ des points d'avantage est appelée Résistance au
stress (RS).

Le processus général est le suivant :

1 - Les joueurs déterminent un objectif global et leur tactique.

2 - Le score de la capacité utilisée au début du concours (après une
augmentation) donne la résistance au stress du compétiteur.

3 - Les compétiteurs agissent dans l'ordre de leur score de départ.

4 - Lorsqu'un compétiteur remporte une victoire contre son adversaire,
le perdant réduit sa résistance au stress du montant suivant :

VICTOIRE MARGINALE : 5 SP

VICTOIRE MINEURE : 10 SP

VICTOIRE MAJEURE : 15 SP

VICTOIRE COMPLÈTE : 25 SP

4 - Une fois que la réserve unique de RS d'un compétiteur est réduite
à 0 ou moins, le compétiteur est exclu du concours.

5 - La conséquence infligée au perdant est déterminée par le tableau
suivant :

Balance SR (Conséquence) (Niveau de victoire)

0 à -3 (Blessé) (Marginal)

-4 à -6 (affaibli) (mineur)

-7 à -10 (Blessé) (Majeur)

-11 ou moins (Mourir) (Complet)

Il y a plus de détails basés sur les règles des concours de
QuestWorlds et les règles de HQ1.

Je pense que les joueurs auront plus de facilité avec quelque chose
qui leur est plus familier, quelque chose qui ressemble aux points de
vie. Avec une meilleure maîtrise du système, je pense qu'ils seront
plus à même de jouer avec. Ce qui est également vrai pour moi. Je
testerai ce nouveau système cet automne lorsque je reprendrai la
campagne Borderlands & Beyond interrompue par les différents
lockdowns.

** jrutila

L'avantage des points de résolution, c'est que le concours sera
terminé en neuf tours maximum. Et c'est le pire des cas. Dans mon
expérience, c'est généralement quelque chose comme cinq rounds
maximum. Si vous n'utilisez le concours étendu qu'une ou deux fois
pendant la session, pour les concours dans lesquels les joueurs
s'investissent vraiment, avoir quelques jets supplémentaires ne
devrait pas être un problème.

Lors de la narration d'un round en concours étendu, vous disposez de
nombreux éléments : le prix final, les tactiques utilisées pendant le
round, le résultat du round, la nouvelle situation de RP, les autres
concours. La nouvelle situation de RP est extrêmement
précieuse. L'adversaire est-il à une victoire marginale de la victoire
? Cela donnerait-il un résultat Marginal ou Majeur pour l'ensemble du
concours prolongé ? Tous ces aspects devraient donner lieu à une
narration différente. Prenez votre temps pour raconter les manches du
concours étendu. C'est peut-être le concours le plus important de vos
sessions, après tout.

Maintenant, à propos de votre mécanisme de points de stress (j'aime ce
nom). Les valeurs de réduction statiques ne rendraient-elles pas le
concours plus long au fur et à mesure que les valeurs de départ sont
plus importantes ? Par exemple (je ne sais pas si quelqu'un joue
encore avec des valeurs aussi grandes) 5W2 contre 3W2, cela pourrait
durer longtemps. Bien sûr, les maîtrises pourraient s'annuler, mais le
match se terminerait alors sur un résultat marginal.

L'autre chose à prendre en compte est l'avantage supplémentaire. Dans
un cas de 14 contre 15W, la valeur 14 a déjà un désavantage sur les
jets. Avoir 20 points de stress en moins ne fait qu'ajouter à cela. Ce
même point peut être fait pour la séquence pariée dans les règles de
QW, cependant.

** jajagappa

Je suis d'accord.  Presque tous se terminent en moins de 5 rounds.  Et
ils sont toujours dramatiques - le point central de pivot d'un
scénario et la conclusion.  Ce sont des événements dont on se
souvient.

** Corvantir

Merci pour les réponses.

Les concours étendus sont peu nombreux dans ma campagne, presque tous
les concours sont résolus par des concours simples.

Les concours étendus que j'ai gérés jusqu'à présent ont pris plus
d'une demi-heure, et même plus, au moins deux d'entre eux étaient trop
longs pour tout le monde.

Deux des trois groupes avec lesquels j'ai testé HQG ne voulaient pas
connaître le nombre précis de PR obtenus par les concurrents, y
compris eux-mêmes. Le système devait s'effacer derrière l'écran. Mais
sans un moyen de "visualiser" la situation (le nombre de PR marqués),
je suppose que les malentendus sont plus fréquents. Cela met beaucoup
de poids sur les épaules du MJ, tout le monde doit comprendre la
situation à travers le MJ uniquement. Comme le cerveau humain est bon
pour analyser des représentations graphiques simples, voir les bilans
de RP tout en écoutant le MJ permet au joueur de saisir rapidement
l'étendue réelle de la situation.

Je pense que ce système fonctionne vraiment si les joueurs peuvent
voir les traces des RP. Une fois que les RP sont inscrits sur quelque
chose que tout le monde peut voir, le MJ peut décrire la situation
sans parler de chiffres.

Malheureusement, je n'ai pas de tableau blanc et ma campagne actuelle
est en ligne. Oublions donc cette solution.

IMO, le système hybride permet au joueur d'appréhender instantanément
sa situation en regardant simplement l'équilibre de résistance au
stress de son personnage. Du côté du joueur, il n'y a qu'un seul score
à gérer. En ce qui me concerne, j'ai du mal à gérer les différentes
pistes de RP des concours étendus de HQG et cela devient de plus en
plus difficile avec chaque nouvelle paire de participants. Un seul
score de résistance au stress par participant simplifie grandement le
processus de gestion, ce qui est très important pour moi.

Les pertes de points de stress ont été calculées de manière à ce
qu'une victoire totale mette hors jeu un concurrent sous 6W. Entre 19
et 5W, le système hybride produit les mêmes résultats qu'un seul pool
de PR. Je suis d'accord pour dire que des scores plus élevés
prolongent la durée de la compétition, mais un seul pool de résistance
au stress accélère la compétition. Les combats fictifs que j'ai faits
en testant en solo le système hybride ont résolu le concours en 15 à
20 mn alors que le concours étendu sur lequel ils étaient basés
prenait plus d'une heure, peut-être une heure et demie. Je suppose que
ce sera plus long à la table mais que cela ne devrait pas dépasser une
demi-heure.

Mes futurs tests montreront si le système hybride et ses options
fonctionnent. Il faudra cependant un certain temps pour apprendre
comment il se comporte face à des scores plus élevés.

** Corvantir

Un rapport tardif !

Nous avons terminé notre campagne Hollow Earth Expedition en juillet
et avons testé le système hybride lors de la session climatique. Il
s'est avéré beaucoup plus facile à gérer de mon point de vue et les
joueurs l'ont trouvé meilleur et plus "naturel" que les systèmes RP et
AP. Les termes "points de stress" se sont imposés assez naturellement
dans la conversation, mieux que les points de résolution et les points
d'avantage. Peut-être parce que nous savons instinctivement ce qu'est
le stress et parce que nous réagissons fortement au stress en général.

Pour moi, la sensation du concours étendu était clairement un
croisement entre HQ1 et HQ2. Aucun défaut apparent pour le moment.

Les joueurs ont trouvé des raccourcis qui leur ont permis d'éviter
l'opposition physique, à l'exception d'un seul concours
étendu. D'ailleurs, le premier concours était prévu pour être
relativement facile afin de prendre la main sur le système. La session
restait héroïque et risquée mais ne permettait qu'une expérimentation
de base.

Je dirais que ce système hybride fonctionne pour nous mais qu'il doit
être confirmé sur le long terme.

** Mugen

Une conséquence de cette règle est que le temps de résolution du
concours est lié à la capacité de chaque concurrent, ce que vous
pouvez considérer comme une caractéristique de votre système et
quelque chose que vous souhaitez.

Cependant, je me souviens avoir lu quelque part qu'une conséquence du
système d'enchères était que les personnages aux capacités faibles
avaient tendance à enchérir fortement, car leur meilleure chance de
gagner était de réduire le nombre d'échanges. Les personnages plus
forts, en revanche, profiteront davantage des enchères basses. Une
solution avec des pertes fixes comme la vôtre n'est pas sujette à un
tel problème.

Mais j'essaierais quand même d'introduire un "risque" dans le système,
en utilisant une sorte de système de "stances", comme dans Pendragon
ou MouseGuard. Soit sous la forme d'une modification de la perte de
SP/AP (par exemple, utiliser un multiple de 6 au lieu de 5 pour les
pertes si le personnage était agressif, ou 4 s'il était défensif),
soit par un système de bonus/malus sur les capacités, basé sur un
système pierre/papier/ciseau/(puits ?)/(lézard/Spock ?).

** Corvantir

Les options suivantes sont à la disposition des joueurs, elles sont
tirées de HQ2 :

Réponses défensives -> Souffrir -5 SP en cas de défaite ; Infliger -10
SP en cas de victoire.

Gambits risqués -> La victoire rapporte +5 SP ; la défaite entraîne
+10 SP.

** Corvantir

Bonjour, j'espère que vous allez tous bien en ces temps étranges.

Il est temps de faire une mise à jour. Je suis passé par une campagne
"Hollow Earth Expedition" dans laquelle j'ai commencé quelques
expériences et tests.

Je mène actuellement deux campagnes HQG (Borderlands & Beyond, deux
groupes différents) avec une version stable du système hybride. J'ai
retravaillé mes aides de jeu, principalement en concevant un nouveau
tableau avec des résultats directement lisibles pour tous les
sous-systèmes du concours (tests simples, tests simples de groupe et
tests étendus).

En ce qui concerne mes besoins, les tests sont maintenant réussis.

Les résultats sont faciles à lire et à appliquer. L'attrition des
points de stress introduit une tension renforçant la narration. Comme
les points de stress ne sont pas des points de vie, je dois décrire
une situation exprimant un danger, un soulagement, quelque chose qui
empire ou s'améliore, à un endroit donné. Cela me pousse à raconter le
résultat d'un échange au lieu d'appeler le personnage suivant juste
après que les points de stress aient été traités. Le résultat fade
(vous perdez 5 points de stress) est suivi d'une description, d'un
retour à la fiction. Cela fonctionne pour moi.

Les résultats sont également beaucoup plus faciles à suivre pour
toutes les personnes présentes à la table et cela aide beaucoup. Grâce
aux éléments narratifs, il me semble que les joueurs ne considèrent
plus les points de stress comme des points de vie. Le stress semble
être le terme approprié.

C'est maintenant mon YHQGWV.


