#!/bin/sh -

pandoc -o notes_univers_troika.pdf notes_univers_troika.md &&
pandoc -o notes_univers_troika.epub notes_univers_troika.md &&

pandoc -o troika_homerules.pdf troika_homerules.md &&
pandoc -o troika_homerules.epub troika_homerules.md 
