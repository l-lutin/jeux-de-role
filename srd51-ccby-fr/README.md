# SRD 5.1 en français

Il s'agit du SRD 5.1 de D&D 5e [convertit en markdown par oznogon](https://github.com/oznogon/cc-srd) et traduit en français par **Deepl**.

Il s'agit d'une base de travail pour une traduction potentielle sérieuse.

**Faites-en ce que vous voulez.**

Ce qu'il faudrait faire :

- Découper le document en parties.

- Le mettre sur un dépôt dédié (là il s'agit d'un sous-dossier de mes petits projets persos)

- Vérifier et consolider la traduction (le gros du boulot)

- Revoir l'encodage des tableaux qui ne me semble pas idéal (à voir)
